# Contributing

## Python version

Python 3.7, Python 3.8, Python 3.9 

## Coding style

+ The code base uses [black](https://github.com/psf/black) with line length of 120 chars max. 
+ Typing is encouraged ! It must be implemented, using [code hinting](https://docs.python.org/3/library/typing.html).

## git management

+ The project uses [git-flow](https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow) workfow.
+ Concise ane precise comments on commits are required. Regular commits and push on this repository (at least 1/day)...   
+ All developments are made in the `develop` branch.
+ Releases are in the `master` branch, with tags.
+ A PR each thursday night. A code review will be performed every fridays. If OK, commits will go to `master`.

## Pre-commit hooks

+ https://github.com/pre-commit/pre-commit-hooks
  + trailing-whitespace
  + end-of-file-fixer
  + check-json
  + check-yaml
  + mixed-line-ending (lf)
+ mypy
+ Black
+ flake8
  - flake8-flask
  - flake8-bugbear
  - flake8-builtins
  - flake8-comprehensions
  - flake8-cognitive-complexity
  - flake8-alfred
  - flake8-logging-format
  - flake8-printf-formatting
  - flake8-pep3101
  - flake8-docstrings
  - flake8-rst-docstrings

## Tests

The project uses [pytest](https://docs.pytest.org/en/stable/) as tests framework. 

## Docs

Code documentation generation uses [Sphinx](https://www.sphinx-doc.org/en/master/). 

All public classes / methods / functions must be documented.

## Project tree structure

+ flask-gallery/ (GIT root directory)
  + ansible/ # Deployment
    + vars/
      + main.yml # deployment vars
      + secrets.yml # Vault encoded secret
    + galerie.tourcoing.fr # Main deployment playbook
  + docs
    + source/ # docs source root
  + scripts/ # Shell scripts (if needed)
  + requirements/ # Requirement files
    + common.txt # PRODUCTION requirements, with version numbers
    + develop.txt # DEVELOPMENT additional requirements
    + flake8.txt  # Flake8 code check dependencies
    + tests.txt # Tests dependencies
  + src/ # Sources root
    + flask_gallery/ # Main package
      + \_\_init\_\_.py  # Only version number, "0.1.0" at start
      + \_\_main\_\_.py  # Package main file
      + cli.py           # CLI commands
      + ...
      + webapp/
       + \_\_init\_\_.py  # Flask application initialization
       + ...
  

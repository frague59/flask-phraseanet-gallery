# flask-phraseanet-gallery

A [Flask](https://flask.palletsprojects.com/en/1.1.x/) based gallery project to display some images from a [Phraseanet 4.x](https://www.phraseanet.com/) server.

The project uses public RSS publication flows to get the images to display, many RSS flows can be displayed.

## Introduction

This project aims to provide a Gallery of pictures from [Phraseanet](https://www.phraseanet.com/), an open source solution for digital asset management.

It includes two components: 

+ a **grabber**, which gets periodically the RSS meta and images URLs, and store the result into a SQLIte database (for now) , 
+ A **Web application** which displays the pictures in a "pretty" view.

## Requirements

+ Globals
  + [peewee](http://docs.peewee-orm.com/en/latest/) for database management (ORM)

+ Grabber
  + [Click](https://click.palletsprojects.com/en/7.x/) for CLI integration
  + [requests](https://fr.python-requests.org/en/latest/) for HTTP requests
  + [requests-oauthlib](https://requests-oauthlib.readthedocs.io/en/latest/) for HTTP requests authentication

+ WebApp
  + HTML5 / CSS / JS (?)
  + [Flask](https://flask.palletsprojects.com/en/1.1.x/)
  + [Flask-Bootstrap4](https://github.com/goodtiding5/flask-bootstrap4)
    
## Deployment

The deployment uses [ansible](https://www.ansible.com/); I'll provide playbooks.
